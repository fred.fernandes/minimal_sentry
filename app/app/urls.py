from django.contrib import admin
from django.urls import include, path

import test_app.urls

admin.site.site_header = 'App Admin'

urlpatterns = [
    path('test/', include(test_app.urls)),
    path('admin/', admin.site.urls),
]
