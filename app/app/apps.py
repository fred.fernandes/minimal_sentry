from django.apps import AppConfig


class TestAppConfig(AppConfig):
    name = 'app'
    verbose_name = "App"
