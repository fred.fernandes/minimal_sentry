Django>=3.2.0,<3.3.0
psycopg2-binary>=2.9.1
sentry-sdk==1.5.8
graphene-django==2.15.0
django-environ==0.8.1
