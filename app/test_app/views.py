import logging

import sentry_sdk
from django.http import HttpResponse

from test_app.models import TestModel


def test(request):
        with sentry_sdk.Hub.current.scope.span.start_child() as t:
                obj = TestModel.objects.first()
                if not obj:
                        TestModel(label="first instance").save()
                        obj = TestModel.objects.first()
                logging.error("test error")
        return HttpResponse(obj.label)


