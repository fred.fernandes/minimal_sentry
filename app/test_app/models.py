from django.db import models
from django.utils.translation import gettext_lazy as _


class TestModel(models.Model):
    label = models.CharField(_('label'), max_length=20, null=True, blank=True)

    class Meta:
        ordering = ('-id',)
