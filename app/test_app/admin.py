from django.contrib import admin

from test_app.models import TestModel


@admin.register(TestModel)
class BookEntryAdmin(admin.ModelAdmin):
    pass
